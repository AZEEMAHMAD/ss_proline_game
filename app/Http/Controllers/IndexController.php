<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;

class IndexController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function guard()
    {
        return Auth::guard('web');
    }
    public function login()
    {
        return view('user.login');
    }

    public function userregisterartion(Request $request){
        $this->validate($request, [
            //'email' => 'required|email',
           // 'name'=>'required|string',
            'mobile'=>'required|digits:10|numeric',
        ]);
          $data=$request->except(['email','name','_token']);
          $checkmobile=User::where('mobile',$data['mobile'])->first();
          if($checkmobile){
              $this->guard()->login($checkmobile);
              return redirect($this->redirectTo);

          }
          else{
//             $this->validate($request, [
//            'email' => 'required|email|unique:users',
//            ]);
             try {
            $user= User::create($data);
            $this->guard()->login($user);
            return redirect($this->redirectTo);
         } catch (\Illuminate\Database\QueryException $ex) {

        }
          }

    }
}
