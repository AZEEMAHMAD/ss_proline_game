<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Proline</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet prefetch" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">
    <link rel="stylesheet" href="css/app.css">
</head>
<body>

<div class="container">
    <header>
        <img src="img/logo.png">
    </header>

    <section class="qrimg">
        <h3>Scan the QR code to play the game</h3>
        <a href="form.html"><img src="img/qrimg.png"></a>

    </section>



</div>
<script src="js/app.js"></script>
</body>
</html>
